clearvars; close all; delete('*,asv'); clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% All units are in meters and meters/sec, instead of mu = 3.986e5 km^3/s^3,
% it is 3.986e14 m^3/s^2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This code is provided 'as-is' for use with AOE 5234: Orbital Mechanics.
% Numerical accuracy is guaranteed only to within the bounds specified by
% The MathWorks Inc.
%
% Author: Andrew Rogers, 10 February 2015
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% GA-STM
Req     = 6378.1363e3; % Radius of Earth (meters)
mu      = 3.986004415e14; % Gravitational parameter (m^3/s^2)
% J2      = 1082.629e-6; % J2 coefficient
J2 = 0;
tol     = 1e-12; % tolerance for transcendental root finding
safetyAltitude = 50e3;

% Valid descriptions are 'Classical'; 'Nonsingular'
chiefOrbitDescription = 'Classical';
% Valid descriptions are 'Cartesian'; 'Relative Classical'; 'Relative Nonsingular'
deputyOrbitDescription = 'Cartesian';

[chiefOrbitDescription,deputyOrbitDescription] = checkDescriptors(chiefOrbitDescription,deputyOrbitDescription);

% Note: Classical with Cartesian is the most stable currently

method = chiefOrbitDescription;
switch method
    case 'Classical'
        a = 7000e3;
        ecc = 0.05;
        inc = 45*pi/180;
        raan = 0;
        w = 0;
        M0 = 0;
        n = sqrt(mu/a^3);
        Elements = [a ecc inc raan w M0]';
    case 'Nonsingular'
         a = 8494.549e3;
         th = 170.003*pi/180;
         inc = 69.988*pi/180;
         q1 = 9.420e-2;
         q2 = 3.407e-2;
         raan = 45.006*pi/180;
         n = sqrt(mu/a^3);
         Elements = [a th inc q1 q2 raan]';
         ecc = sqrt(q1^2 + q2^2);         
end

method = deputyOrbitDescription;
switch method
    case 'Cartesian'
        eccFactor = -n*(2+ecc)/(sqrt((1+ecc)*(1-ecc)^3));
        x0 = 400;
        y0 = 400;
        z0 = 100;
        xd0 = -1;
        yd0 = eccFactor*x0; 
        zd0 = 0;
        RelInitState = [x0 xd0 y0 yd0 z0 zd0]';
    case 'Relative Classical'
        da = 0;
        de = 0.0001;
        di = 0.2*pi/180;
        dO = 0*pi/180;
        dw = 0;
        dM = 0*pi/180;
        RelInitState = [da de di dO dw dM]';
    case 'Relative Nonsingular'
        da = -103.624;
        dth = -1.104e-3;
        di = 7.7076e-4;
        dq1 = 4.262e-5;
        dq2 = -9.708e-6;
        dO = 3.227e-3;
        RelInitState = [da dth di dq1 dq2 dO]';
end

t0 = 0; numPeriod = 1;

initStruct.params = {Req,mu,J2,tol,t0,numPeriod,safetyAltitude};
initStruct.initChiefDescription = chiefOrbitDescription;
initStruct.initDeputyDescription = deputyOrbitDescription;
initStruct.RelInitState = RelInitState(:);
initStruct.Elements = Elements(:);

GA = GimAlfriendSTM(initStruct);
GA.makeTimeVector();
GA.PropagateGASTM();
% GA.plotGAOrbit();

%% LERM/NERM

% Set up Chief orbit with orbital elements
caseNum   = 'Case 4';
if strcmp(caseNum,'Case 1') == 1
    a      = 7000e3;
    ecc    = 0.0;
    inc    = 45*pi/180;
    raan   = pi/6;
    argper = pi/6;
    f0     = 0;
elseif strcmp(caseNum,'Case 2') == 1
    a      = 15000e3;
    ecc    = 0.4;
    inc    = 45*pi/180;
    raan   = pi/4;
    argper = pi/6;
    f0     = 0;
elseif strcmp(caseNum,'Case 3') == 1
    a      = 45000e3;
    ecc    = 0.8;
    inc    = 45*pi/180;
    raan   = pi/4;
    argper = pi/6;
    f0     = 0;
else
    a      = 7000e3;
    ecc    = 0.05;
    inc    = 45*pi/180;
    raan   = 0;
    argper = 0;
    f0     = 0;
end

n = sqrt(mu/a^3);

% % Set up deputy initial conditions
% Bounded relative motion constraint factor (dimensionless)
eccFactor = -n*(2+ecc)/(sqrt((1+ecc)*(1-ecc)^3));

% Relative offsets from Chief (meters)
x0 = 400;
y0 = 400;
z0 = 100;

% Relative velocities from Chief (meters/second)
xd0 = -1;
yd0 = eccFactor*x0;
zd0 = 0;

% Initial condition vector for deputy satellite
XT_INIT = [x0 y0 z0 xd0 yd0 zd0]';

% Initial time
t0 = 0;
% Number of orbital periods
numPeriod = 1;
% Integrator options
options = odeset('RelTol',3e-12,'AbsTol',1e-15,'Stats','on');

initStruct.params{1} = mu;
initStruct.params{2} = [a ecc inc raan argper f0]';
initStruct.params{3} = XT_INIT;
initStruct.timeParams{1} = t0;
initStruct.timeParams{2} = numPeriod;
initStruct.options = options;

States = {RelativeMotion(initStruct),RelativeMotion(initStruct)};
States{1}.propagateLinear();
States{2}.propagateNonlinear();

%% Broucke STM (LERM)
period = 2*pi/n;
tf = numPeriod*period;
tspan = linspace(t0,tf,numPeriod*200);
Phi = BrouckeSTM(tspan,mu,Elements,tol);
X0 = [x0 y0 xd0 yd0 z0 zd0]';
X_STM = zeros(6,200);
for ii = 1:length(tspan)
    X_STM(:,ii) = Phi(:,:,ii)*X0;
end

%% Plotting
figure
hold on
grid on
if J2 ~= 0
    title1 = title('$J_2$-Perturbed Relative Motion');
else
    title1 = title('Unperturbed Relative Motion');
end
xl = xlabel('Radial, $x$, m');
yl = ylabel('In-track, $y$, m');
zl = zlabel('Cross-track, $z$, m');
set([title1,xl,yl,zl],'interpreter','latex','fontsize',12);
plot3(States{1}.XL(:,1),States{1}.XL(:,2),States{1}.XL(:,3),'k','linewidth',2)
plot3(States{2}.XN(:,1),States{2}.XN(:,2),States{2}.XN(:,3),'ro','markersize',6)
plot3(X_STM(1,:),X_STM(2,:),X_STM(5,:),'g','linewidth',2)
plot3(GA.X(1,:),GA.X(3,:),GA.X(5,:),'b*','markersize',3)
legend('LERM','NERM','Broucke-STM','Gim-Alfriend STM','Location','Best');
axis tight
hold off
