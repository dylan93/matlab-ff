function Phi = BrouckeSTM(tspan,mu,ChiefKeplerElem,tol)
% Author: Dylan Thomas
%
% Function to implement State-Transition Matrix (STM) for the Linear
% Equations of Relative Motion (LERM) in continuous-time (Broucke, 2003).
% Allows one to propogate relative state vector in LVLH frame forward in
% time without numeric integration.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Inputs:
%           tspan
%           mu
%           
% Outputs:
%
%
%
%

% Retrieive needed orbital elements
a       = ChiefKeplerElem(1);
ecc     = ChiefKeplerElem(2);
omega   = ChiefKeplerElem(5);
% Calculate convenient constansts
n       = sqrt(mu/a^3);
eta     = sqrt(1-ecc^2);
p       = a*(1-ecc^2);
% Determine Fundamental Matrix Inverse at initial time
t0                  = tspan(1);
[r0,~,theta,~,~,E0] = ffcoef(t0,mu,a,ecc,omega,tol);
f0                  = theta - omega;
[RIxy,RIz]          = RInverseMatrix(t0,mu,a,ecc,r0,f0,E0,n,eta,p);

% Initialize STM
Phi = zeros(6,6,length(tspan));
for ii = 1:length(tspan)
    % Determine change in time from initial to current time
    dt                      = tspan(ii)-t0;
    
    % Retrieve coefficients at current time
    [r,~,theta,~,~,EccAnom] = ffcoef(dt,mu,a,ecc,omega,tol);
    f                       = theta - omega;
    % Calculate Fundamental Matrix at current time
    [Rxy,Rz]                = Rmatrix(dt,mu,a,ecc,r,f,EccAnom,n,eta,p);
    % Get the in-plane & out-of-plane STM's
    Phixy                   = Rxy*RIxy;
    Phiz                    = Rz*RIz;
    % Concatenate into full STM
    Phi(:,:,ii)             = [Phixy zeros(4,2); zeros(2,4) Phiz];
end
end

function [Rxy,Rz] = Rmatrix(dt,mu,a,ecc,r,f,EccAnom,n,eta,p)
% Calculates the Fundamental Matrices at a time dt from t_0

% Special condition for circular orbits
if ecc == 0
    M = EccAnom;
    % In-plane Fundamental Matrix
    Rxy = [1            -cos(M)     sin(M)      0;
           -3*n*dt*0.5  2*sin(M)    2*cos(M)    1;
           0            n*sin(M)    n*cos(M)    0;
           -3*n*0.5     2*n*cos(M)  -2*n*sin(M) 0];
else
    % In-plane Fundamental Matrix columns
    Rxy(:,1) = [(r/a) - (3*n*dt*ecc*sin(f))/(2*eta);
                -3/2*n*dt*eta*(a/r);
                -(n*ecc*sin(f))/(2*eta) - 3/2*dt*ecc*cos(f)*(n*a/r)^2;
                3/2*dt*ecc*sin(f)*(n*a/r)^2 - 3/2*eta*(n*a/r)];
    
    Rxy(:,2) = [-cos(f);
                (1+r/p)*sin(f);
                n*sin(f)*eta*(a/r)^2
                n*eta*(1+r/p)*(a/r)^2*cos(f) + (ecc*n*sin(f)^2)/sqrt((1-ecc^2)^3)];
    
    Rxy(:,3) = [ecc*sin(f)/eta;
                eta*(a/r);
                ecc*n*cos(f)*(a/r)^2;
                -ecc*n*sin(f)*(a/r)^2];
    
    Rxy(:,4) = [0;
                r/a;
                0;
                (ecc*n*sin(f))/eta];
end
% Components for the out-of-plane solution
z1      = sqrt(a*n/mu)*r*cos(f); 
z2      = sqrt(a*n/(mu*(1-ecc^2)))*r*sin(f);
z1Dot   = -a*sqrt(n)*sin(EccAnom)/r; z2Dot = a*sqrt(n)*cos(EccAnom)/r;
% Create out-of plane fundamental matrix
Rz      = [z1, z2; z1Dot, z2Dot];
end

function [RIxy,RIz] = RInverseMatrix(dt,mu,a,ecc,r,f,EccAnom,n,eta,p)
% Calculates the inverse Fundamental Matrices at a time dt from t_0

% Special condition for circular orbits
if ecc == 0
    M = EccAnom;
    % In-plane Inverse Fundamental Matrix
    RIxy = [4           0 0         2/n;
            3*cos(M)    0 sin(M)/n  2*cos(M)/n;
            -3*sin(M)   0 cos(M)/n  -2*sin(M)/n;
            6*n*dt      1 -2/n      3*dt];
else
    % Sub-matrices which compose the full inverse fundamental matrix
    Imatrix = [2*(a/r)^2*(1+p/r),       (a/r)*(3*cos(f) + ecc*(2+cos(f)^2));
                -2*(a/r)^2*ecc*sin(f),  -ecc*sin(f)*(a/p)*(ecc+cos(f)*(2+ecc*cos(f)));
                (2*ecc*sin(f))/(n*eta), (eta/n)*sin(f);
                2*(a/r)*(eta/n),        (eta/n)*(cos(f)+(r/p)*(ecc+cos(f)))];
    
    Jmatrix = [(-a/p)*sin(f)*(ecc^2+(1+ecc*cos(f))*(3+ecc*cos(f))), -(a^2/(p*r))*ecc*sin(f)*(2+(p/r)+(r/p));
                (a/p)*ecc*sin(f)^2*(2+ecc*cos(f)),                  (a/p)^2*(1+ecc*cos(f) - ecc^2*(ecc*cos(f)^3+2*cos(f)^2-1));
                (r*eta)/(p*n)*(cos(f)+ecc*(cos(f)^2-2)),            (ecc*cos(f)-1)/(n*eta)*(1+(r/p));
                -(eta/n)*sin(f)*(1+(r/p)),                          -(ecc*sin(f))/(n*eta)*(1+(r/p))];
    
    Kmatrix = [((n*ecc)/eta)*(a/r)*(1+(p/r)),   (n/sqrt((1-ecc^2)^3))*(a/r)^2*(1+(p/r));
                -(n*ecc^2*sin(f))/eta*(a/r)^2, -(n*ecc*sin(f))/sqrt((1-ecc^2)^3)*(a/r)^2;
                (ecc^2*sin(f))/(1-ecc^2),       (ecc*sin(f))/((1-ecc^2)^2);
                (a*ecc)/r,                      a/((1-ecc^2)*r)];
    % In-plane Fundamental Matrix
    RIxy = transpose([Imatrix, Jmatrix + 3*dt*Kmatrix]);
end
% Components for the out-of-plane solution
z1      = sqrt(a*n/mu)*r*cos(f); 
z2      = sqrt(a*n/(mu*(1-ecc^2)))*r*sin(f);
z1Dot   = -a*sqrt(n)*sin(EccAnom)/r; z2Dot = a*sqrt(n)*cos(EccAnom)/r;
% Create out-of plane fundamental matrix
RIz     = [z2Dot, -z2; -z1Dot, z1];

end

function [r,rdot,theta,thetadot,thetaddot,EccAnom] = ffcoef(dt,mu,a,ecc,omega,tol)
% Time-varying Formation Flying coefficients derived by parameterizing the 
% orbit equation in polar form and treat the problem as planar. 

n = sqrt(mu/a^3);
M = n*dt;
[EccAnom,f] = kepler(M,ecc,tol);
r = (a.*(1-ecc.^2))./(1+ecc.*cos(f));
thetadot = sqrt(mu.*a.*(1-ecc.^2)).*(1+ecc.*cos(f)).^2./(a.^2.*(1-ecc.^2).^2);
rdot = ecc.*sin(f).*sqrt(mu.*a.*(1-ecc.^2))./(a.*(1-ecc.^2));
thetaddot = -2.*rdot.*thetadot./r;

theta       = omega + f;

% Equations for coefficients
% r           = a*(1-ecc^2)/(1+ecc*cos(theta));
% rdot        = sqrt(mu*a*(1-ecc^2))*(1+ecc*cos(theta))^2/(a^2*(1-ecc^2)^2);
% thetadot    = ecc*mu*sin(theta)/(sqrt(mu*a*(1-ecc^2)));
% thetaddot   = 2*mu*ecc*(1+ecc*cos(theta))^3*sin(theta)/(a^3*(1-ecc^2)^3);
end

function [E, f] = kepler(M, e, Tol1)

% Kepler's Equation
E = M;
FF = 1;
while abs(FF) > Tol1
    FF = M - (E - e*sin(E));
    dFFdE = -(1 - e*cos(E));
    del_E = -FF / dFFdE;
    E = E + del_E;
end
while (E < 0)
    E = E + (2*pi);
end
while (E >= (2*pi))
    E = E - (2*pi);
end

%
kk_plus = 0;
while (M < 0)
    kk_plus = kk_plus + 1;
    M = M + (2*pi);
end
kk_minus = 0;
while (M >= (2*pi))
    kk_minus = kk_minus + 1;
    M = M - (2*pi);
end

% True Anomaly
f = 2*atan(sqrt((1+e)/(1-e))*tan(E/2));
if (E >= 0) && (E <= (pi))
    f = abs(f);
else
    f = (2*pi) - abs(f);
end;

f = f - kk_plus*(2*pi) + kk_minus*(2*pi);

end
%%% Old Kepler function
% function [E,iter] = kepler(M,ecc,tol)
% % Function solves Kepler's equation:
% % M = n*(t-t_0) = E-e*sin(E)
% % Using Newton-Raphson iteration
% % AC Rogers, 21 February 2013
% % Inputs:
% %           M    = mean anomaly
% %           e    = eccentricity
% % Outputs:
% %           E    = eccentric anomaly
% %           iter = number of iterations
% format long
% iter    = 1;
% for ii = 1:length(M)
%     E_n = M(ii);
%     f_n = E_n-ecc.*sin(E_n) - M(ii);
%     while (abs(f_n) > tol)
%         E_n = E_n - f_n./(1-ecc.*cos(E_n));
%         f_n = E_n - ecc.*sin(E_n) - M(ii);
%         iter = iter + 1;
%     end
%     E(ii) = E_n;
% end
% format short
% end