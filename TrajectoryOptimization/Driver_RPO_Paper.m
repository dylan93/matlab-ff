%%% Satellite Relative Motion Optimal Trajectory Generation

% Driver for SciTech RPO Paper

% Calcs Opt Trajectory for 20 different initial distance to the same final
% postion using GASTM. Does one for Radial/In-track/Cross-track separations
% and at four different eccentricities. Saves data in separate files.

% Super lazy & inefficient

%% e = 0

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clearvars; close all; clc; asv(); addPaths(); 
%%% Global Constants
Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization2();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control

dX0 = linspace(1000,50000,20);

for ii = 1:length(dX0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dX0));
    x0 = dX0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

Name1 = 'GASTM';
save('Radial_Maneuver_e0_8000.mat')
clearvars;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control


Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization2();

dY0 = linspace(1000,50000,20);

for ii = 1:length(dY0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dY0));
    y0 = dY0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

save('In-Track_Maneuver_e0_a8000.mat')
clearvars;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control


Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization2();

dZ0 = linspace(1000,50000,20);

for ii = 1:length(dZ0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dZ0));
    z0 = dZ0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

fprintf('Finished case e = %f',ecc)
save('Cross-Track_Maneuver_e0_a8000.mat')
clearvars;

%% ecc=0.1

%%% Global Constants
Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization3();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control

dX0 = linspace(1000,50000,20);

for ii = 1:length(dX0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dX0));
    x0 = dX0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

Name1 = 'GASTM';
save('Radial_Maneuver_e01_a8000.mat')
clearvars;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control

Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization3();

dY0 = linspace(1000,50000,20);

for ii = 1:length(dY0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dY0));
    y0 = dY0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

save('In-Track_Maneuver_e01_a8000.mat')
clearvars;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control

Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization3();

dZ0 = linspace(1000,50000,20);

for ii = 1:length(dZ0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dZ0));
    z0 = dZ0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

fprintf('Finished case e = %f',ecc)
save('Cross-Track_Maneuver_e01_a8000.mat')
clearvars;

%% ecc=0.2

%%% Global Constants
Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization4();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control

dX0 = linspace(1000,50000,20);

for ii = 1:length(dX0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dX0));
    x0 = dX0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

Name1 = 'GASTM';
save('Radial_Maneuver_e02_a8000.mat')
clearvars;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control

Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization4();


dY0 = linspace(1000,50000,20);

for ii = 1:length(dY0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dY0));
    y0 = dY0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

save('In-Track_Maneuver_e02_a8000.mat')
clearvars;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control

Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization4();


dZ0 = linspace(1000,50000,20);

for ii = 1:length(dZ0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dZ0));
    z0 = dZ0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

fprintf('Finished case e = %f',ecc)
save('Cross-Track_Maneuver_e02_a8000.mat')
clearvars;

%% ecc=0.3

%%% Global Constants
Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization5();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control

dX0 = linspace(1000,50000,20);

for ii = 1:length(dX0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dX0));
    x0 = dX0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

Name1 = 'GASTM';
save('Radial_Maneuver_e03_a8000.mat')
clearvars;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control

Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization5();

dY0 = linspace(1000,50000,20);

for ii = 1:length(dY0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dY0));
    y0 = dY0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

save('In-Track_Maneuver_e03_a8000.mat')
clearvars;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control

Req     = 6378.1363e3;      % Radius of Earth (meters)
mu      = 3.986004415e14;   % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization5();

dZ0 = linspace(1000,50000,20);

for ii = 1:length(dZ0)
    fprintf('\nInitial condition %i out of %i \n',ii,length(dZ0));
    z0 = dZ0(ii);
    % Setup the GA problem. Chose options/variables
    GASTM_Config();
    controller{ii} = ConvexSFFManeuverRPO(initStruct);
    controller{ii}.minimizeFuel();
    
    deltaV(ii) = controller{ii}.optimalObjective;
end

fprintf('Finished case e = %f',ecc)
save('Cross-Track_Maneuver_e03_a8000.mat')
clearvars;

Driver_RPO_Paper_Plot();