%%% SciTech RPO Paper Plot Driver

clearvars; close all; clc;

addpath('Data');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('Cross-track_Maneuver_e0_a7000.mat')

figure
hold on
plot(dZ0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'k*')
grid on
xlabel('Initial Cross-Track Difference \deltaZ_0, km')
ylabel('Fuel mass consumumption, %')
clearvars

load('Cross-track_Maneuver_e01_a7000.mat')

plot(dZ0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'b*')

clearvars

load('Cross-track_Maneuver_e02_a7000.mat')

plot(dZ0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'g*')

clearvars

load('Cross-track_Maneuver_e03_a7000.mat')

plot(dZ0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'r*')

clearvars
legend('e=0','e=0.1','e=0.2','e=0.3','Location','Best')
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('In-track_Maneuver_e0_a7000.mat')

figure
hold on
plot(dY0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'k*')
grid on
xlabel('Initial In-Track Difference \deltaY_0, km')
ylabel('Fuel mass consumumption, %')
clearvars

load('In-track_Maneuver_e01_a7000.mat')

plot(dY0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'b*')

clearvars

load('In-track_Maneuver_e02_a7000.mat')

plot(dY0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'g*')

clearvars

load('In-track_Maneuver_e03_a7000.mat')

plot(dY0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'r*')

clearvars
legend('e=0','e=0.1','e=0.2','e=0.3','Location','Best')
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('Radial_Maneuver_e0_a7000.mat')

figure
hold on
plot(dX0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'k*')
grid on
xlabel('Initial Radial Difference \deltaX_0, km')
ylabel('Fuel mass consumumption, %')
clearvars

load('Radial_Maneuver_e01_a7000.mat')

plot(dX0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'b*')

clearvars

load('Radial_Maneuver_e02_a7000.mat')

plot(dX0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'g*')

clearvars

load('Radial_Maneuver_e03_a7000.mat')

plot(dX0/1000,100*deltaV/(9.81*(203)*log(14/12.5)),'r*')

clearvars
legend('e=0','e=0.1','e=0.2','e=0.3','Location','Best')
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clearvars
load('Radial_Maneuver_e02_a7000.mat')

% Setup plotting information/variables in this script
Plot_Config_RPO();

% Instantiate plot class
plotMotion = OrbitPlotter(inputStruct);
% Plot the relative orbits
plotMotion.plot3DOrbit();
% Plot the control histories
plotMotion.plotControls();
% Plot the state histories
plotMotion.plotStates();