%%% Satellite Relative Motion Optimal Trajectory Generation


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clearvars; close all; clc; asv(); addPaths(); 
%%% Global Constants
Req     = 6378.1363e3; % Radius of Earth (meters)
mu      = 3.986004415e14; % Gravitational parameter (m^3/s^2)

%%% Time parameters - user specified (3 options):
% 
% 1) Specify simulation time of flight directly in seconds.
%
% 2) Specify number of orbital periods to simulate.
%
% 3) Use default time of one orbital period.

% Time step for simulation (sec). 
dt = 1;
% Time of flight of simulation (sec). Integer or empty array.
tf = 800;
% Number of orbital periods. Null if tf isn't empy. Integer or empty array.
numPeriod = [];

%%% Other User-defined variables
% Verbose amount for Gurobi output (0,1,2)
verbose = 1;

% tolerance for transcendental root finding
tol = 1e-13; 

% Number of time samples between each time step for discretization
samples = 3;

% Maximum Thrust Inputs
umax = 2.79*8/14;
umin = -umax;

Initialization();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Gim-Alfriend STM Optimal Control

% Setup the GA problem. Chose options/variables
GASTM_Config();
% Instantiate maneuver class
controller{1} = ConvexSFFManeuver(initStruct);
% Perform trajectory optimization
controller{1}.minimizeFuel();
Name1 = 'GASTM';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% LERM Optimal Control

% Setup the GA problem. Chose options/variables
LERM_Config();
% Instantiate maneuver class
controller{2} = ConvexSFFManeuver(initStruct);
% controller{2}.motionModel.propagateState();
% Perform trajectory optimization
controller{2}.minimizeFuel();
Name2 = 'LERM';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% HCW Optimal Control

% % Setup the HCW problem. Chose options/variables
% HCW_Config();
% % Instantiate maneuver class
% controller{2} = ConvexSFFManeuver(initStruct);
% % Perform trajectory optimization
% controller{2}.minimizeFuel();
% Name2 = 'HCW';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Plotting & Results

% Setup plotting information/variables in this script
Plot_Config_OptControl();

% Instantiate plot class
plotMotion = OrbitPlotter(inputStruct);
% Plot the relative orbits
plotMotion.plot3DOrbit();
% Plot the control histories
plotMotion.plotControls();
% Plot the state histories
plotMotion.plotStates();

% Check diff in propagation/influence
% Akdiff=controller{1}.motionModel.Ak-controller{2}.motionModel.Ak;
% max(max(max(abs(Akdiff))))
% Bkdiff=controller{1}.motionModel.Bk-controller{2}.motionModel.Bk;
% max(max(max(abs(Bkdiff))))