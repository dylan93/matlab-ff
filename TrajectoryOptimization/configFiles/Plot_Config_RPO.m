% Setting up plotting class for ScitTechRPO Paper

% Config file
nCont = length(controller);
inputStruct.states.states = {controller{10}.X,controller{10}.X0,controller{10}.Xf};
inputStruct.states.statesq = {controller{10}.Xq};
inputStruct.controls.controls = {controller{10}.U};
inputStruct.controls.controlsq = {controller{10}.Uq};
inputStruct.times = {controller{10}.T};
inputStruct.id = {Name1,Name1,Name1};
inputStruct.lines.linestates = num2cell(cool(nCont), 2);
inputStruct.lines.linemods = {'linewidth','markersize','markersize'};
inputStruct.lines.linesizes = [1,7,7];
inputStruct.lines.linestatesq = num2cell(cool(nCont), 2);
inputStruct.lines.linemodsq = {'linewidth'};
inputStruct.lines.linesizesq = [1];
inputStruct.legends = {'GASTM Min Fuel','$X_0$','$X_f$','Thrust'};
inputStruct.title = 'Relative Trajectory';
inputStruct.labels = {'Radial, $X$, m','In-Track, $Y$, m','Cross-Track, $Z$, m'};
inputStruct.bounds = 'tight';
inputStruct.shuttleFlag = 'no';
inputStruct.umax = controller{10}.umax;

