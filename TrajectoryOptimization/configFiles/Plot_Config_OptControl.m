% Setting up plotting class

nCont = length(controller);
states = cell(nCont+2,1);
statesq = cell(nCont,1);
controls = cell(nCont,1);
controlsq = cell(nCont,1);
times = cell(nCont,1);
legend = cell(2*nCont+2,1);

linestates = num2cell(cool(nCont), 2);
linestates{end+1} = [0 0 0];
linestates{end+1} = [1 1 1];
linemods = cell(nCont+2,1);
linemods{end} = 'markersize';
linemods{end-1} = 'markersize';
linesizes = zeros(nCont+2,1);
linesizes(end) = 7;
linesizes(end-1) = 7;
for jj = 1:1:nCont
    states{jj} = controller{jj}.X;
    statesq{jj} = controller{jj}.Xq;
    controls{jj} = controller{jj}.U;
    controlsq{jj} = controller{jj}.Uq;
    times{jj} = controller{jj}.T;
    legend{jj} = {Name{jj}};
    linemods{jj} = 'linewidth';
    linesizes(jj) = 1;
end

leg = cat(2,legend{:},{'$X_0$'},{'$X_f$'},legend{:});

states{nCont+1} = controller{1}.X0;
states{nCont+2} = controller{1}.Xf;
inputStruct.states.states = states;
inputStruct.states.statesq = statesq;
inputStruct.controls.controls = controls;
inputStruct.controls.controlsq = controlsq;
inputStruct.times = times;
inputStruct.id = cat(2, strcat(Name), Name{1}, Name{1});
inputStruct.lines.linestates = linestates;
inputStruct.lines.linemods = linemods;
inputStruct.lines.linesizes = linesizes;
inputStruct.lines.linestatesq = linestates;
inputStruct.lines.linemodsq = linemods;
inputStruct.lines.linesizesq = linesizes;
inputStruct.legends = leg;
inputStruct.title = 'Relative Trajectory';
inputStruct.labels = {'Radial, $X$, m','In-Track, $Y$, m','Cross-Track, $Z$, m'};
inputStruct.bounds = 'tight';
inputStruct.shuttleFlag = 'no';
inputStruct.umax = controller{1}.umax;

