%%%%% Configuration script for HCW Model

% J2 coefficient
if strcmp(J2flag,'off') == 1
    J2  = 0; % Un-perturbed motion
elseif strcmp(J2flag,'on') == 1
    J2  = 1082.629e-6; % Perturbed motion
else
    error('Input error: Incorrect J2 Flag')
end

% Determine number of control directions & B matrix
switch numControlInput
    case 2
        B = [zeros(4,2); 
            eye(2)];
    case 3
        B = [zeros(3,3); 
            eye(3)];
end

% Extra params
n = sqrt(mu/a^3);
period = 2*pi/n;
eccFactor = -2*n;

% Time parameters
if isempty(tf) == 0
    nothing();
elseif isempty(numPeriod) == 0
    tf = numPeriod*period;
else
    tf = period;
end

% Bounded motion
if strcmp(BndMotionFlag,'True') == 1
    yd0 = eccFactor*x0; 
    ydf = eccFactor*xf;
elseif strcmp(BndMotionFlag,'False') == 1
    nothing();
else
    error('Input error: Incorrect Bounded Motion Flag')
end

method = chiefOrbitDescription;
switch method
    case 'Classical'
        Elements = [a ecc inc raan w M0]';
    case 'Nonsingular'
        Elements = [a th inc q1 q2 raan]';
end

% Initial deputy orbit description
method = deputyOrbitDescriptionInit;
switch method
    case 'Cartesian'
        RelInitState = [x0 y0 z0 xd0 yd0 zd0]';
end

% Final deputy orbit description
method = deputyOrbitDescriptionFinal;
switch method
    case 'Cartesian'
        RelFinalState = [xf yf zf xdf ydf zdf]';
end

% Create struture to initialize model class
initStruct.descriptor = 'HCW';
initStruct.params = {mu,a,RelInitState};
initStruct.terminalCondition = RelFinalState;
initStruct.timeParams.t0 = t0;
initStruct.timeParams.dt = dt;
initStruct.timeParams.tf = tf;
initStruct.maneuverParams = {samples,B,umax,umin};
initStruct.verbose = verbose;
initStruct.quivFreq = quivFreq;