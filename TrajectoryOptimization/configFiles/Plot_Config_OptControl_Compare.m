% Setting up plotting class

nCont = length(controller);
inputStruct.states.states = {controller{1}.X,controller{2}.X,controller{1}.X0,controller{1}.Xf};
inputStruct.states.statesq = {controller{1}.Xq,controller{2}.Xq};
inputStruct.controls.controls = {controller{1}.U,controller{2}.U};
inputStruct.controls.controlsq = {controller{1}.Uq,controller{2}.Uq};
inputStruct.times = {controller{1}.T,controller{2}.T};
inputStruct.id = {Name1,Name2,Name1,Name1};
inputStruct.lines.linestates = num2cell(cool(nCont), 2);
inputStruct.lines.linemods = {'linewidth','linewidth','markersize','markersize'};
inputStruct.lines.linesizes = [1,1,7,7];
inputStruct.lines.linestatesq = num2cell(cool(nCont), 2);
inputStruct.lines.linemodsq = {'linewidth','linewidth'};
inputStruct.lines.linesizesq = [1,1];
inputStruct.legends = {'GASTM Min Fuel','HCW Min Fuel','$X_0$','$X_f$','GATSM Min Fuel Thrust','HCW Min Fuel Thrust'};
inputStruct.title = 'Relative Trajectory';
inputStruct.labels = {'Radial, $X$, m','In-Track, $Y$, m','Cross-Track, $Z$, m'};
inputStruct.bounds = 'tight';
inputStruct.shuttleFlag = 'no';
inputStruct.umax = controller{1}.umax;

